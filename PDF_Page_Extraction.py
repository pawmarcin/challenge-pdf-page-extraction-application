import easygui as gui
from PyPDF4 import PdfFileReader, PdfFileWriter

class Extract:
    def __init__(self):
        self.input_path = None
        self.start_page = None
        self.end_page = None
        self.output_path = None

    def select_pdf_file(self):
        self.input_path = gui.fileopenbox(title="Select a PDF to extract", default="*.pdf")
        if self.input_path is None:
            exit()

    def get_start_page(self):
        while True:
            self.start_page = gui.enterbox(msg="Enter first page :", title="First page")
            if self.start_page is None:
                exit()
            try:
                self.start_page = int(self.start_page)
                if self.start_page <= 0:
                    gui.msgbox("Application needs number of the page")
                else:
                    break
            except ValueError:
                gui.msgbox("Application needs number of the page")

    def get_end_page(self):
        while True:
            self.end_page = gui.enterbox(msg="Enter last page you want to extract", title="Last Page")
            if self.end_page is None:
                exit()
            try:
                self.end_page = int(self.end_page)
                if self.end_page <= 0:
                    gui.msgbox("Application needs number of the page")
                    continue
                if self.end_page < self.start_page:
                    gui.msgbox("Last page number cannot be less than first page number.")
                    continue
                break
            except ValueError:
                gui.msgbox("Application needs number of the page")

    def select_output_location(self):
        save_title = "Save the extracted PDF as..."
        file_type = "*.pdf"
        self.output_path = gui.filesavebox(title=save_title, default=file_type)
        if self.output_path is None:
            exit()

    def extract_pages(self):
        input_pdf = PdfFileReader(open(self.input_path, "rb")) 
        # Must remember to check if PDF FIle is empty or not. 
        # If wrong used it will clean the PDF File Completely
        output_pdf = PdfFileWriter()
        
        for page_num in range(self.start_page - 1, min(self.end_page, input_pdf.numPages)):
            output_pdf.addPage(input_pdf.getPage(page_num))
        with open(self.output_path, "wb") as output_file:
            output_pdf.write(output_file)
        gui.msgbox("Pages extracted in new file!")


if __name__ == "__main__":
    extractor = Extract()
    extractor.select_pdf_file()
    extractor.get_start_page()
    extractor.get_end_page()
    extractor.select_output_location()
    extractor.extract_pages()
